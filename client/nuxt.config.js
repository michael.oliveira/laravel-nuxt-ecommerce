require('dotenv').config()

import i18n from './config/i18n'

export default {

	// mode: 'universal',

	server: {
			port: process.env.PORT,
			host: process.env.HOST,
			serverUrl: process.env.API_URL
		},

		env: {
		serverUrl: process.env.API_URL
	},

	/*
	** Headers of the page
	*/
	head: {
		title: process.env.npm_package_name || '',
		meta: [
		{ charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
		{ hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
		],
		link: [
		{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},
	/*
	** Customize the progress-bar color
	*/
	loading: { color: '#fff' },
	/*
	** Global CSS
	*/
	css: [
    // '~/assets/css/app.scss'
  ],
	/*
	** Plugins to load before mounting the App
	*/
	plugins: [
    // { src: '~/plugins/axios.js' },
    { src: "~/plugins/vue-js-modal", mode: "client" },
    { src: '~/plugins/localStorage', ssr: false },
    '~/plugins/formatter',
    '~/plugins/mask',
    '~/plugins/axios'
  ],
	/*
	** Nuxt.js dev-modules
	*/
	buildModules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/fontawesome'
	],

	tailwindcss: {
		configPath: '~/config/tailwind.config.js',
		cssPath: '~/assets/css/tailwind.css',
		exposeConfig: false
	},
	/*
	** Nuxt.js modules
	*/
	modules: [
		'@nuxtjs/axios',
    '@nuxtjs/auth',
		'@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/toast',
    'nuxt-buefy',
    'nuxt-fontawesome',
    // '@nuxtjs/proxy',
    // 'cookie-universal-nuxt',
    [
      'nuxt-i18n',
      // vueI18nLoader: true,
      {
        defaultLocale: 'pt',
        locales: [
          {
            code: 'en',
            name: 'English',
          },
          {
            code: 'pt',
            name: 'Portugues',
          },
        ],
        vueI18n: i18n,
        parsePages: false,
        // pages: {
        //   'product/_id': {
        //     pt: '/product/:id?',
        //     en: '/product/:id?',
        //   },
        //   // '_nested/_route/_': {
        //   //   en: '/mycustompath/:nested/*', // * will match the entire route path after /:nested/
        //   // },
        // },
      },
    ],
  ],

  buefy: {
    materialDesignIcons: false,
    defaultIconPack: 'fas',
    defaultIconComponent: 'font-awesome-icon'
  },

  fontawesome: {
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      }
    ]
  },

  toast: {
    position: 'top-right',
    duration: 2000
  },

  axios: {
    proxy: true,
    credentials: true   // Attention, credentials not withCredentials
  },

  proxy: {
    '/root/': {
      target: process.env.API_URL,
      pathRewrite: {"^/root/": ""},
    },
    '/api/': { target: process.env.API_URL + '/api', pathRewrite: {'^/api/': ''} },

    // '/app/': {
    //   target: process.env.API_URL,
    //   pathRewrite: {"^/app/": "/api/"},
    // },
    '/api1/': {
      target: 'http://ws.correios.com.br/calculador/calcPrecoPrazo.aspx',
      pathRewrite: {"^/api1": ""},
      changeOrigin: true
    },
    '/consultacep/': {
      target: 'https://viacep.com.br/ws',
      pathRewrite: {"^/consultacep": ""},
      changeOrigin: true
    }
  },

  fontawesome: {
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      }
    ]
  },

  auth: {
    cookie: {
      options: {
        secure: true
      },
    },
    redirect: {
      logout: '/',
      callback: false,
      login: false,
      home: false
    },
    // watchLoggedIn: true,
    // rewriteRedirects: false,
    strategies: {

      local: {
        endpoints: {
          login: {
            url: `${process.env.API_URL}/login`,
            method: 'post',
            withCredentials: true,
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              'Content-Type': 'application/json'
            },
            propertyName: false
          },
          logout: {
            url: `${process.env.API_URL}/logout`,
            method: 'post',
            withCredentials: true,
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              'Content-Type': 'application/json'
            },
            propertyName: false
          },
          // user: false
          user: {
            url: `${process.env.API_URL}/api/user`,
            method: 'get',
            propertyName: false,
            withCredentials: true,
            headers: {
              // 'Referer': process.env.BASE_URL,
              'X-Requested-With': 'XMLHttpRequest',
              'Content-Type': 'application/json'
            }
          }
        },
        tokenRequired: false,
        tokenType: false
      }
    },
    localStorage: false,
    plugins: [ '~/plugins/auth' ]
  },

	router: {
		middleware: ['auth']
	},

	/*
	** Build configuration
	*/
	build: {
		/*
		** You can extend webpack config here
		*/
		/*
    ** Run ESLint on save
    */
   extend (config, ctx) {
      config.node = {
        fs: 'empty'
      }
    },
    transpile: ['@stylelib']
	}
}
