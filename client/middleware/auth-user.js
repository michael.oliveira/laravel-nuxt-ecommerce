export default async function ({ app, $auth, route, store, redirect }) {

  if (!$auth.loggedIn) {

    const from = route.fullPath || route.path

    redirect('/login', { ...route.query, redirect: from })

  } else {

  }
}

