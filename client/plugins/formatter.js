const currencyFormatted = (value) => {
    
    let formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',

    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
    });

    return formatter.format(value);
}

const getAmountValue = (value) => {
    return value.replace('R$', '').replace('.','').replace(',','.').trim()
}

export default ({ app }, inject) => {
    inject('currencyFormatted', currencyFormatted)
    inject('getAmountValue', getAmountValue)
    // context.$currencyFormatted = currencyFormatted

    // getAmountValue
};