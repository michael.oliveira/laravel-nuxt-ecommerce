export default function({ $axios, store, redirect }){

    // $axios.setHeader('Content-Type', 'application/x-www-form-urlencoded', [
    //     'post'
    // ])

    $axios.onRequest(request => {

    })

    $axios.onError(error => {
      const code = parseInt(error.response && error.response.status)
      if (code === 400) {
        redirect('/400')
      } else if (code === 401) {
        const from = route.fullPath || route.path

        redirect('/login', { ...route.query, redirect: from })
      }
    });
}
