document.addEventListener('DOMContentLoaded', (event) => {

	var submenuDirection = ( !document.querySelector('.cd-dropdown-wrapper').classList.contains('open-to-left') ) ? 'right' : 'left';

	const options = {
		activate: function(row) {
			for (let element of row.children) {
				element.classList.add('is-active')
				element.classList.remove('fade-out')
				if (element.querySelector('ul'))
				{
					element.querySelector('ul').classList.add('fade-in')
				}
			}
        },
        deactivate: function(row) {
			for (let element of row.children) {
				element.classList.remove('is-active')
				// element.classList.remove('is-active')
				if (element.querySelector('ul'))
				{
					element.querySelector('ul').classList.add('fade-out')
				}
			}
        },
        exitMenu: function() {
			document.querySelectorAll('.cd-dropdown-content').forEach(element => {
				element.classList.remove('is-active')
			})
        	return true;
        },
        submenuDirection: submenuDirection,
	}

	document.querySelector('.cd-dropdown-content').addEventListener('mouseenter', (event) => {
		menuAim(options);
	})

const wrapper = document.querySelector('.cd-main-content')
const dropdownTrigger = document.querySelector('.cd-dropdown-trigger')
const dropdown = document.querySelector('.cd-dropdown')
const goBack = document.querySelectorAll('.go-back')
const hasChildren = document.querySelectorAll('.has-children > a')
hasChildren.forEach((element, i) => {
	element.addEventListener('click', (event) => {
		event.preventDefault()
		element.nextElementSibling.classList.remove('is-hidden')
		element.parentNode.parentNode.classList.add('move-out')
	})
})

goBack.forEach((element, i) => {
	element.addEventListener('click', function(){
		element.parentNode.classList.add('is-hidden')
		element.parentNode.parentNode.parentNode.classList.remove('move-out')
	}); 
})

function toggleNav(){
	var navIsVisible = ( !dropdown.classList.contains('dropdown-is-active') ) ? true : false;
	dropdown.classList.toggle('dropdown-is-active', navIsVisible);
	dropdownTrigger.classList.toggle('dropdown-is-active', navIsVisible);
	if( !navIsVisible ) {
		dropdown.addEventListener('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
			document.querySelector('.has-children ul').classList.add('is-hidden');
			document.querySelector('.move-out') && document.querySelector('.move-out').classList.remove('move-out');
			document.querySelector('.is-active') && document.querySelector('.is-active').classList.remove('is-active');
		});	
	}
}

//close meganavigation
document.querySelector('.cd-dropdown .cd-close').addEventListener('click', (event) => {
	event.preventDefault();
	toggleNav();
});

dropdownTrigger.addEventListener('click', (event) => {
	event.preventDefault();
	toggleNav();
});

wrapper.addEventListener('click', (event) => {
	event.preventDefault()
	if (dropdown.classList.contains('dropdown-is-active')) {
		toggleNav()
	}
})

})