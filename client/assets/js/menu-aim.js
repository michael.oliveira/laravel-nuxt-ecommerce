

    menuAim = function(opts) {
        // Initialize menu-aim
        
        init.call(this, opts);

        return this;
    };

    function init(opts) {
        var activeRow = null,
            mouseLocs = [],
            lastDelayLoc = null,
            timeoutId = null,
            options = Object.assign({}, {
                rowSelector: ".cd-dropdown-content > li",
                submenuSelector: "*",
                submenuDirection: "right",
                tolerance: 75,  // bigger = more forgivey when entering submenu
                enter: () => {},
                exit: () => {},
                activate: () => {},
                deactivate: () => {},
                exitMenu: () => {}
            }, opts);
        var MOUSE_LOCS_TRACKED = 3,  // number of past mouse locations to track
            DELAY = 300;  // ms delay when user appears to be entering submenu

        /**
         * Keep track of the last few locations of the mouse.
         */
        const mousemoveDocument = function(e) {
            mouseLocs.push({x: e.pageX, y: e.pageY});
            
            if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
                mouseLocs.shift();
            }
        };

        /**
         * Cancel possible row activations when leaving the menu entirely
         */
        const mouseleaveMenu = function() {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }

            // If exitMenu is supplied and returns true, deactivate the
            // currently active row on menu exit.
            if (options.exitMenu(this)) {
                if (activeRow) {
                    options.deactivate(activeRow);
                }

                // activeRow = null;
            }
        };

        /**
         * Trigger a possible row activation whenever entering a new row.
         */
        const mouseenterRow = function(row) {
            if (timeoutId) {
                // Cancel any previous activation delays
                clearTimeout(timeoutId);
            }

            options.enter(this);
            possiblyActivate(this);
        },

        mouseleaveRow = function() {
            options.exit(this);
        };

        /*
         * Immediately activate a row if the user clicks on it.
         */
        const clickRow = function() {
                activate(this);
            };

        /**
         * Activate a menu row.
         */
        const activate = function(row) {
            if (row == activeRow) {
                return;
            }

            if (activeRow) {
                options.deactivate(activeRow);
            }

            options.activate(row);
            activeRow = row;
        }

        const possiblyActivate = function(row) {
            var delay = activationDelay()

            if (delay) {
                timeoutId = setTimeout(function() {
                    possiblyActivate(row)
                }, delay)
            } else {
                activate(row)
            }
        }

        const matches = function(el, selector) {
            return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
        };

        const activationDelay = function() {
            if (!activeRow || !matches(activeRow, options.submenuSelector)) {
                // If there is no other submenu row already active, then
                // go ahead and activate immediately.
                console.log(activeRow)
                return 0;
            }
            var offset = activeRow.getBoundingClientRect(),
                upperLeft = {
                    x: offset.left,
                    y: offset.top - options.tolerance
                },
                upperRight = {
                    x: offset.left + activeRow.offsetWidth,
                    y: upperLeft.y
                },
                lowerLeft = {
                    x: offset.left,
                    y: offset.top + activeRow.offsetHeight + options.tolerance
                },
                lowerRight = {
                    x: offset.left + activeRow.offsetWidth,
                    y: lowerLeft.y
                },
                loc = mouseLocs[mouseLocs.length - 1],
                prevLoc = mouseLocs[0];

            if (!loc) {
                return 0;
            }

            if (!prevLoc) {
                prevLoc = loc;
            }

            if (prevLoc.x < offset.left || prevLoc.x > lowerRight.x ||
                prevLoc.y < offset.top || prevLoc.y > lowerRight.y) {

                return 0;
            }

            if (lastDelayLoc &&
                    loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {

                return 0;
            }

            function slope(a, b) {
                return (b.y - a.y) / (b.x - a.x);
            };

            var decreasingCorner = upperRight,
                increasingCorner = lowerRight;

            // Our expectations for decreasing or increasing slope values
            // depends on which direction the submenu opens relative to the
            // main menu. By default, if the menu opens on the right, we
            // expect the slope between the cursor and the upper right
            // corner to decrease over time, as explained above. If the
            // submenu opens in a different direction, we change our slope
            // expectations.
            if (options.submenuDirection == "left") {
                decreasingCorner = lowerLeft;
                increasingCorner = upperLeft;
            } else if (options.submenuDirection == "below") {
                decreasingCorner = lowerRight;
                increasingCorner = lowerLeft;
            } else if (options.submenuDirection == "above") {
                decreasingCorner = upperLeft;
                increasingCorner = upperRight;
            }

            var decreasingSlope = slope(loc, decreasingCorner),
                increasingSlope = slope(loc, increasingCorner),
                prevDecreasingSlope = slope(prevLoc, decreasingCorner),
                prevIncreasingSlope = slope(prevLoc, increasingCorner);

            if (decreasingSlope < prevDecreasingSlope &&
                    increasingSlope > prevIncreasingSlope) {
                // Mouse is moving from previous location towards the
                // currently activated submenu. Delay before activating a
                // new menu row, because user may be moving into submenu.
                lastDelayLoc = loc;
                return DELAY;
            }

            lastDelayLoc = null;
            return 0;
        }

        /**
         * Hook up initial menu events
         */
        document.querySelector('.cd-dropdown-content').addEventListener('mouseleave', mouseleaveMenu)
        document.querySelectorAll(options.rowSelector).forEach(element => {
            element.addEventListener('mouseenter', mouseenterRow)
            element.addEventListener('mouseleave', mouseleaveRow)
            element.addEventListener('click', clickRow)
        })

        document.addEventListener('mousemove', mousemoveDocument);

    }