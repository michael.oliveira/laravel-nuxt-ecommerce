import queryString from 'query-string'

export const state = () => ({
  parent: null,
  variants: [],
  variant: null
})

export const getters = {
  parent (state) {
    return state.parent
  },

  variants (state) {
    return state.variants
  },

  variant (state) {
    return state.variant
  }
}

export const mutations = {
  SET_PARENT (state, parent) {
    state.parent = parent
  },

  SET_VARIANTS (state, variants) {
    state.variants = variants
  },

  SET_CURRENT_VARIANT (state, variant) {
    state.variant = variant
  }
}

export const actions = {
  async searchVariants({ commit }, data) {
    const apiUrl = process.env.API_URL
    // await this.$axios.$get('/root/sanctum/csrf-cookie', {
    //   headers: {
    //     'content-type': 'application/json',
    //     'accept': 'application/json'
    //   },
    //   withCredentials: true
    // }).then(async response => {
    //   console.log('carregou')
      let variations = await this.$axios.$post('/api/products-variants', { data: { ...data } } )
      if (variations.data.length === 1) {
        commit('SET_CURRENT_VARIANT', variations.data[0])
      } else {
        commit('SET_VARIANTS', variations.data)
      }
    // })
  },

  setParent({ commit }, data) {
    commit('SET_PARENT', data)
    commit('SET_VARIANTS', data.variations)
    commit('SET_CURRENT_VARIANT', data.variations.find(variant => variant.default === true))
  }
}
