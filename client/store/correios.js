const getDefaultState = () => {
    return {
        service: []
    }
}

const state = () => getDefaultState()

const getters = {
    service (state){
        return state.service
    }
}

const mutations = {
    SET_SERVICE(state, service){
        state.service = [...state.service, { service }]
    },
    resetState (state) {
        Object.assign(state, getDefaultState())
    }
}

const actions = {
    setService({ commit }, service){
        commit('SET_SERVICE', service)
    },

    async getService ({ commit}, cep) {
        let response;
        
        response = await this.$axios.$post(`/app/shipping/frete`, {
            'cep': cep
        })
        
        commit('SET_SERVICE', response)
        
        return response;
    },

    clearServices({ commit }){
        commit('resetState')
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}