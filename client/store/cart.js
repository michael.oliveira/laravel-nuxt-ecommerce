import queryString from 'query-string'

export const state = () => ({
    products: [],
    empty: true,
    totalItemCart: null,
    subtotal: null,
    total: null,
    changed: false,
    shipping: null,
    addresses: [],
    paymentMethod: null
})

export const getters = {
    products (state) {
        return state.products
    },

    addresses (state) {
        return state.addresses
    },

    totalItemCart (state) {
        return state.totalItemCart
    },

    count (state) {
        return state.products.length
    },

    empty (state) {
        return state.empty
    },

    subtotal (state) {
        return state.subtotal
    },

    total (state) {
        return state.total
    },

    changed (state) {
        return state.changed
    },

    shipping (state) {
        return state.shipping
    },

    paymentMethod (state){
        return state.paymentMethod
    }
}

export const mutations = {
    SET_PRODUCTS (state, products) {
        state.products = products
    },

    SET_ADDRESSES (state, addresses) {
        state.addresses = addresses
    },

    SET_EMPTY (state, empty) {
        state.empty = empty
    },

    SET_TOTAL_ITEM_CART (state, totalItemCart){
        state.totalItemCart = totalItemCart
    },

    SET_SUBTOTAL (state, subtotal) {
        state.subtotal = subtotal
    },

    SET_TOTAL (state, total) {
        state.total = total
    },

    SET_CHANGED (state, changed) {
        state.changed = changed
    },

    SET_SHIPPING (state, shipping) {
        state.shipping = shipping
    },

    SET_PAYMENT_METHOD (state, paymentMethod){
        state.paymentMethod = paymentMethod
    },

    ADD_PRODUCT(state, product) {
      state.products = [...state.products, product ]
    }
}

export const actions = {

    async getCart ({ commit, state }) {
        let query = {}
        let response;
        if (state.shipping){
            query.shipping_method_id = state.shipping.id
        }

        if (!this.$auth.loggedIn) {
            response = await this.$axios.$get(`/api/cart-session?${queryString.stringify(query)}`)
        } else {
            response = await this.$axios.$get(`/api/cart?${queryString.stringify(query)}`)
        }

        commit('SET_PRODUCTS', response.data.products)
        commit('SET_EMPTY', response.meta.empty)
        commit('SET_SUBTOTAL', response.meta.subtotal)
        commit('SET_TOTAL', response.meta.total)
        commit('SET_CHANGED', response.meta.changed)
        commit('SET_SHIPPING', response.meta.shipping)

        return response;
    },

    async clearCart({ commit, dispatch }){

        await this.$axios.$get('/api/clear-cart').then(() => {
            dispatch('getCart')
        })
    },

    async destroy ({ dispatch }, productId) {
      if (this.$auth.loggedIn){
        await this.$axios.$delete(`/api/cart/${productId}`)
      } else {
        await this.$axios.$delete(`/api/cart-session/${productId}`)
      }
        this.$toast.success("Produto excluido do carrinho com sucesso!")

        dispatch('getCart')
    },

    async changeAddressDefault({ dispatch }, addressId){
        await this.$axios.$post(`/api/address-change-default/${addressId}`, {
            default: true
        })
    },

    async changePaymentDefault({ dispatch }, paymentMethodId){
        await this.$axios.$post(`/api/payment-change-default/${paymentMethodId}`, {
            default: true
        }).then(data => {
            console.log('Forma de pagamento atualizada')
        })
    },

    async updateShipping({ dispatch }, shippingId){
        await this.$axios.$patch(`/api/shipping-methods/${shippingId}`, {
            default: true
        })
            .then(result => {
                console.log('atualizado')
            })

        dispatch('getCart')
    },

    async update ({ dispatch }, { productId, quantity }) {
        if (!this.$auth.loggedIn) {
            await this.$axios.$patch(`/api/cart-session/${productId}`, {
                quantity
            })
        } else {
          await this.$axios.$patch(`/api/cart/${productId}`, {
              quantity
          })
        }

        dispatch('getCart')
    },

    async setAddresses({ commit }, addresses) {
        commit('SET_ADDRESSES', addresses)
    },

    async setShipping ({ commit }, shipping) {
        commit('SET_SHIPPING', shipping)
    },

    async setPaymentMethod({ commit }, paymentMethod){
        commit('SET_PAYMENT_METHOD', paymentMethod)
    },

    async setTotalItem({ commit }, totalItemCart){
        commit('SET_TOTAL_ITEM_CART', totalItemCart)
    },

    async store ({ dispatch, rootState, commit, state, $getAmountValue }, products) {
        try{
          if (this.$auth.loggedIn){
            await this.$axios.$post('/api/cart', {
              products
            }).then(response => {
              console.log(response)
            })

          } else {
            await this.$axios.$post('/api/cart-session', {
                products
            })
          }

          dispatch('getCart')
          this.$toast.success("Produto adicionado ao carrinho com sucesso!")

        } catch(e) {
            console.log(e)
            this.$toast.error("Erro tentando adicionar item ao carrinho!")
        }

    }
}
