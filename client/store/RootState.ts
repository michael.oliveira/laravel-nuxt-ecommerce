export default interface RootState {
  config: any,
  cart: any,
  checkout: any,
  product: any,
  shipping: any,
  user: any,
  whishlist: any,
  attribute: any,
  category: {
    current_path: string,
    current_product_query: any,
    current: {
      slug: string,
      name: string
    },
    filters: any
  },
  stock: {
    cache: any
  },
  route?: any
}
