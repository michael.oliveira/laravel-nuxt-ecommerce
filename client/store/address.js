export const state = () => ({
    addresses: [],
    meta: {}
})

export const getters = {
    addresses(state) {
        return state.addresses
    },

    meta(state) {
        return state.meta
    }
}

export const mutations = {
    SET_ADDRESSES (state, addresses) {
        state.addresses = addresses
    },

    SET_META (state, meta) {
        state.meta = meta
    },

    SET_CURRENT_PAGE (state, currentPage) {
        state.meta.current_page = currentPage
    },

    SET_PER_PAGE (state, perPage) {
        state.meta.per_page = perPage
    }
}

export const actions = {
    async getAddresses({ state, commit }) {

        let endpoint_address = '/api/addresses'

        if (state.meta.current_page) {
            endpoint_address = endpoint_address + `?page=${state.meta.current_page || 1}`
        }
        if (state.meta.per_page) {
            endpoint_address = endpoint_address + `&per_page=${state.meta.per_page || 5}`
        }

        let response = await this.$axios.$get(endpoint_address)

        commit('SET_ADDRESSES', response.data)
        commit('SET_META', response.meta)

        return response
    },

    async setCurrentPage({ commit, dispatch }, currentPage) {
        commit('SET_CURRENT_PAGE', currentPage)

        dispatch('getAddresses')
    },

    async setPerPage({ commit, dispatch }, perPage) {
        commit('SET_PER_PAGE', perPage)

        dispatch('getAddresses')
    }
}
