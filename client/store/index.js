export const state = () => ({
    categories: {},
    filteredCategories: {},
    featuredProducts: {},
    breadCrumbs: {},
    mpCredentials: {},
    openedNav: {
      element: null,
      status: false
    },
    isMobile: false,
    isTablet: false,
    bannerHero: []
})

export const getters = {
    categories (state) {
        return state.categories
    },

    openedNav (state) {
      return state.openedNav
    },

    mpCredentials (state) {
        return state.mpCredentials
    },

    featuredProducts (state) {
      return state.featuredProducts
    },

    isMobile(state) {
      return state.isMobile
    },

    isTablet(state) {
      return state.isTablet
    },

    bannerHero(state) {
      return state.bannerHero
    },

    filteredCategories(state) {
      return state.filteredCategories
    },

    breadCrumbs(state) {
      return state.breadCrumbs
    }
}

export const mutations = {
    SET_CATEGORIES (state, categories) {
        state.categories = categories
    },

    SET_MP_CREDENTIALS (state, mpCredentials){
        state.mpCredentials = mpCredentials
    },

    SET_OPENED_NAV (state, openedNav) {
      state.openedNav = openedNav
    },

    SET_IS_MOBILE (state, isMobile) {
      state.isMobile = isMobile
    },

    SET_IS_TABLET(state, isTablet) {
      state.isTablet = isTablet
    },

    SET_BANNER_HERO(state, bannerHero) {
      state.bannerHero = bannerHero
    },

    SET_FEATURED_PRODUCTS(state, featuredProducts) {
        state.featuredProducts = featuredProducts
    },

    SET_FILTERED_CATEGORIES(state, filteredCategories) {
      state.filteredCategories = filteredCategories
    },

    SET_BREADCRUMBS(state, breadCrumbs) {
      state.breadCrumbs = breadCrumbs
    }
}

export const actions = {
    async nuxtServerInit({ commit, dispatch, state }, { req }) {

      // await dispatch('cart/getCart')
      let categories = await this.$axios.$get('/api/categories')
      let featuredProducts = await this.$axios.$get('/api/featured-products')

      commit('SET_CATEGORIES', categories.data)
      commit('SET_FEATURED_PRODUCTS', featuredProducts)

    },

    setMpCredentials({ commit }, mpCredentials){
        commit('SET_MP_CREDENTIALS', mpCredentials)
    },

    setOpenedNav({ commit }, value) {
      commit('SET_OPENED_NAV', value)
    },

    setIsMobile({ commit }, isMobile) {
      commit('SET_IS_MOBILE', isMobile)
    },

    setIsTablet({ commit }, isTablet) {
      commit('SET_IS_TABLET', isTablet)
    },

    setBannerHero({ commit }, bannerHero) {
      commit('SET_BANNER_HERO', bannerHero)
    }
}
