export interface ProductOption {
  id: number | string,
  name: string,
  values ?: any[]
}

export interface ProductConfiguration {
  color: ProductOption,
  size: ProductOption
}
