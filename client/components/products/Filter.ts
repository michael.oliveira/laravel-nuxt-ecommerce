import { ProductConfiguration } from './ProductConfiguration'

const getAvailableFiltersByProduct = (product: any) => {
  let filtersMap = {}
  if (product && product.attributes) {
    product.attributes.forEach(attribute => {
      const type = attribute.name
      const filterVariants = attribute.values.map(({ id, name }) => {
        return { id, name, type }
      })
      filtersMap[type] = filterVariants
    })
  }
  return filtersMap
}

const getSelectedFiltersByProduct = (product: any, configuration: ProductConfiguration) => {
  if (!configuration) {
    return null
  }

  let selectedFilters = {}
  if (configuration && product) {
    Object.keys(configuration).map(filterType => {
      const filter = configuration[filterType]
      selectedFilters[filterType] = {
        id: filter.id,
        name: filter.name,
        type: filterType
      }
    })
  }
  return selectedFilters
}

export { getAvailableFiltersByProduct, getSelectedFiltersByProduct }
