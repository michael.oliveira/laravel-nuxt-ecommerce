import pt from '../locales/pt.json'
import en from '../locales/en.json'
import { numberFormats } from './formats/numberFormats'
import { dateTimeFormats } from './formats/dateTimeFormats'

export default {
  locale: 'pt',
  fallbackLocale: 'pt',
  numberFormats,
  dateTimeFormats,
  messages: { en, pt },
}
