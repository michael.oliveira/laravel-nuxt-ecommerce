export const numberFormats = {
  en: {
    currency: {
      style: 'currency',
      currency: 'USD',
    },
  },
  'en-IN': {
    currency: {
      style: 'currency',
      currency: 'INR',
    },
  },
  pt: {
    currency: {
      style: 'currency',
      currency: 'BRL',
    },
  },
}
