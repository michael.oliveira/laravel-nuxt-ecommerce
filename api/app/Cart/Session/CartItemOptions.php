<?php
/**
 * Created by PhpStorm.
 * User: user0
 * Date: 14/10/2018
 * Time: 11:09
 */

namespace App\Cart\Session;


use Illuminate\Support\Collection;

class CartItemOptions extends Collection
{

    /**
     * Get the option by the given key.
     *
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }

}
