<?php

namespace App\Cart\Session\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException {}
