<?php

namespace App\Cart\Session\Exceptions;

use RuntimeException;

class InvalidRowIDException extends RuntimeException {}
