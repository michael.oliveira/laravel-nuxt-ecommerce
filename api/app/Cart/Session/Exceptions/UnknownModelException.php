<?php

namespace App\Cart\Session\Exceptions;

use RuntimeException;

class UnknownModelException extends RuntimeException {}
