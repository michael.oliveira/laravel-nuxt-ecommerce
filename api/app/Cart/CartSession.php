<?php

/**
 * Created by PhpStorm.
 * User: user0
 * Date: 14/10/2018
 * Time: 10:47
 */

namespace App\Cart;

// use \Cart;
use App\Models\ProductVariation;
use App\Models\ShippingMethod;

//use Gloudemans\Shoppingcart\Cart;

use App\Cart\Session\Facades\Cart;
use Illuminate\Support\Str;

class CartSession
{

    protected $changed;

    public function getContent()
    {
        return Cart::content();
    }

    public function add($product)
    {
        $productVariation = ProductVariation::find($product[0]['id']);
        $name = sprintf(
            "%s",
            // $productVariation->product->name,
            // $productVariation->type->name,
            $productVariation->name
        );

        Cart::add($productVariation->id, $name, $product[0]['quantity'], $productVariation->price->amount(), [
            'priceVaries'   => $productVariation->priceVaries(),
            'minStock' => $productVariation->inStock(),
            'stockCount'    => $productVariation->stockCount()
        ])
            ->associate(ProductVariation::class);
    }

    public function withShipping($shippingId)
    {
        $this->shipping = ShippingMethod::find($shippingId);

        return $this;
    }

    public function hasChanged()
    {
        return $this->changed;
    }

    // protected function getStorePayload($products)
    // {
    //     return collect($products)->keyBy('id')->map(function ($product) {
    //         return [
    //             'quantity'  => $product['quantity'] + $this->getCurrentQuantity($product['id'])
    //         ];
    //     })
    //         ->toArray();
    // }

    // public function getCurrentQuantity($productId)
    // {
    //     if ($product = $this->user->cart()->where('id', $productId)->first())
    //     {
    //         return $product->pivot->quantity;
    //     }

    //     return 0;
    // }

    public function isEmpty()
    {
        return Cart::count() === 0;
    }

    public function getCartItem($productId)
    {
        $row = Cart::search(function ($cartItem) use ($productId) {

            if ($cartItem->id === $productId) {
                return $cartItem;
            }
        });

        return $row->first();
    }

    public function delete($productId)
    {
        $row = Cart::search(function ($cartItem) use ($productId) {
            if ($cartItem->id === intval($productId)) {
                return $cartItem;
            }
        })->first();

        Cart::remove($row->rowId);
    }

    public function update($productId, $quantity)
    {
        $row = Cart::search(function ($cartItem) use ($productId) {
            if ($cartItem->id === intval($productId)) {
                return $cartItem;
            }
        })->first();

        Cart::update($row->rowId, $quantity);
    }

    public function empty()
    {
        Cart::destroy();
    }

    public function subtotal()
    {
        return (new Money(Cart::subtotal()));
    }

    public function total()
    {
        return (new Money(Cart::total()));
    }
}
