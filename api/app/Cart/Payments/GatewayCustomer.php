<?php
namespace App\Cart\Payments;

use App\Models\Customer;
use App\Models\PaymentMethod;

interface GatewayCustomer {

    public function charge(PaymentMethod $paymentMethod, $amout, $installments);
    public function addCard($token);
}