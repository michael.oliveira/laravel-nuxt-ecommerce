<?php
namespace App\Cart\Payments;

use App\Models\User;
use Illuminate\Http\Request;

interface Gateway {
    public function withUser(User $user);
    public function createCustomer(Request $request);
    public function getCustomer();
}