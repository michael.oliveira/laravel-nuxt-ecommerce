<?php
namespace App\Cart\Payments\Gateways;

use App\Cart\Payments\Gateway;
use App\Models\User;
use Illuminate\Http\Request;
use PagSeguro;
use MercadoPago;

class MPGateway implements Gateway
{
    public function __construct()
    {
        MercadoPago\SDK::setAccessToken(config('mercadopago.test.token'));
    }

    protected $user;

    public function withUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function createCustomer(Request $request)
    {
        if($this->user->gateway_customer_id){
            return $this->getCustomer();
        }

        $customer = new MPGatewayCustomer(
            $this, $this->createMPCustomer($request)
        );

        return $customer;
    }

    public function getCustomer()
    {
        return new MPGatewayCustomer(
                $this,
                // MercadoPago\Customer::find_by_id($this->user->gateway_customer_id)
                User::with(['addresses' => function ($query) {
                    $query->where('default', true);
                }])->where('gateway_customer_id', $this->user->gateway_customer_id)->first()
        );
    }

    protected function createMPCustomer(Request $request)
    {
        $customer = new MercadoPago\Customer();
        $customer->email = $this->user->email;
        $customer->first_name = $this->user->name;

        $customer->save();

        $request->user()->update([
            'gateway_customer_id'   => $customer->id
        ]);

        return $customer;
    }
}
