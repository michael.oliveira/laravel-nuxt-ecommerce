<?php
namespace App\Cart\Payments\Gateways;

use App\Cart\Payments\Gateway;
use App\Cart\Payments\GatewayCustomer;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Order;
use App\Models\PaymentMethod;
use MercadoPago;

class MPGatewayCustomer implements GatewayCustomer
{
    protected $gateway;
    protected $customer;

    public function __construct(Gateway $gateway, $customer)
    {
        $this->gateway = $gateway;
        $this->customer = $customer;
    }

    public function charge(PaymentMethod $paymentMethod, $amount, $installments)
    {
        $payment = new MercadoPago\Payment();

        $payment->transaction_amount = (float) number_format((float) ((int) $amount/100), 2,'.', '');

        $address = $this->customer->addresses->first();
        $customer = Customer::where('address_id', $address->id)->first();
        $paymentPayer = [];
        $name = $this->split_name($customer->name);

        $payer = [
            'email' => $this->customer->email,
            'first_name' => $name['first_name'],
            'last_name' => $name['last_name'],//$this->costumer->last_name,
            'identification'    => [
                'type'  => $customer->doc_type,//$this->costumer->identification->type,
                'number'    => $customer->cpf_cnpj//$this->costumer->identification->number
            ],
            'address'   => [
                'zip_code' => $address->postal_code,//$this->costumer->address->zip_code,
                'street_name' => $address->address_1,//$this->costumer->address->street_name,
                'street_number' => $address->number,//$this->costumer->address->street_number,
                'neighborhood'  =>  $address->address_2,//$this->costumer->address->address_2,
                'city'  => $address->city,//$this->costumer->address->city,
                'federal_unit'  => $address->state//$this->costumer->address->federal_unit
            ]
        ];
        if ($paymentMethod->type === 'boleto' || $paymentMethod->type === 'on_delivery') {
            $paymentPayer = $payer;
        } else {
            $paymentPayer = array_merge($payer, [
                'type'  => 'customer',
                'id'    => $this->customer->gateway_customer_id
            ]);
            $payment->installments = $installments;
            $payment->issuer_id = $this->getCard($this->customer->gateway_customer_id, $paymentMethod->provider_id)->issuer->id;
            $payment->token = request()->token;

        }
        $payment->external_reference = $paymentMethod->id;

        $payment->description = "Pedido do cliente $customer->name";
        $payment->payment_method_id = $paymentMethod->type === 'boleto' ? 'bolbradesco' : strtolower($paymentMethod->card_type);

        $payment->payer = $paymentPayer;

        $payment->save();

        return $payment;

    }

    public function addCard($request)
    {
        $card = new MercadoPago\Card();
        $card->token = $request->id;
        $card->customer_id = $this->customer->gateway_customer_id;

        $card->save();

        $request->session()->put('issuer', $card->issuer);

        $customer = MercadoPago\Customer::find_by_id($this->customer->gateway_customer_id);

        $customer->default_card = $card->id;
        $customer->update();

        return $card;
    }

    public function getCard($customerId, $cardId) {
        $customer = MercadoPago\Customer::find_by_id($customerId);
        $cards = $customer->cards;
        $card = null;

        foreach($cards as $customerCard) {
            if ($customerCard->id == $cardId)
            {
                $card = $customerCard;
            }
        }

        session()->put('issuer', $card->issuer);

        return $card;
    }

    function split_name($name) {
        $parts = explode(" ", $name);
        $lastname = array_pop($parts);
        while(count($parts) > 1)
        {
            array_pop($parts);
        }
        $firstname = implode(" ", $parts);

        $name = array(
                'first_name' => $firstname,
                'last_name' => $lastname,
        );

        return $name;
    }
}
