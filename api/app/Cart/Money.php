<?php

namespace App\Cart;

use Money\Money as BaseMoney;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Currencies\ISOCurrencies;
use NumberFormatter;

class Money {

    protected $money;
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
        $this->money = new BaseMoney($value, new Currency('BRL'));
    }

    public function amount()
    {
        return $this->money->getAmount();
    }

    public function formatted()
    {
        $formatter = new IntlMoneyFormatter(
            new NumberFormatter('pt_BR', NumberFormatter::CURRENCY),
            new ISOCurrencies()
        );

        return $formatter->format($this->money);
    }

    public function numberFormatter() {
        $formatter = new NumberFormatter('en-US', NumberFormatter::DECIMAL);
        $formatter->setAttribute($formatter::FRACTION_DIGITS, 2);
        return $formatter->format((int) ($this->amount()/100));
    }

    public function add(Money $money){
        $this->money = $this->money->add($money->instance());

        return $this;
    }

    public function instance(){
        return $this->money;
    }
}