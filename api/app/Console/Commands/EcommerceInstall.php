<?php

namespace App\Console\Commands;

use DatabaseSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class EcommerceInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ecommerce:install {--force : Do not ask for user confirmation}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install dummy data for the Ecommerce Application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('force')) {
            $this->proceed();
        } else {
            if ($this->confirm('This will delete ALL your current data and install the default dummy data. Are you sure?')) {
                $this->proceed();
            }
        }
    }

    protected function proceed()
    {
        
        File::deleteDirectory(public_path('storage/products'));
        File::deleteDirectory(public_path('storage/users'));

        $this->callSilent('storage:link');
        $copySuccessProducts = File::copyDirectory(public_path('img/products'), public_path('storage/products/dummy'));
        $copySuccessUsers = File::copyDirectory(public_path('img/users'), public_path('storage/users'));
        if ($copySuccessProducts && $copySuccessUsers) {
            $this->info('Images successfully copied to storage folder.');
        }

        try {
            $this->call('migrate:fresh', [
                '--seed' => true,
                '--force' => true,
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->call('vendor:publish', [
            '--provider' => "Laravel\Scout\ScoutServiceProvider"
        ]);

        $this->call('vendor:publish', [
            '--provider' => "Tymon\JWTAuth\Providers\LaravelServiceProvider"
        ]);

        $this->call('vendor:publish', [
            '--tag'    => 'cors'
        ]);

        $this->call('jwt:secret');
        $this->call('key:generate');
        $this->call('config:cache');

        try {
            // $this->call('scout:clear', [
            //     'model' => 'App\\Models\\Product',
            // ]);

            $this->call('scout:import', [
                'model' => 'App\\Models\\Product',
            ]);
        } catch (\Exception $e) {
            $this->error('Error on installation');
        }

        $this->info('Dummy data installed');
    }
}
