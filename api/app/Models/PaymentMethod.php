<?php

namespace App\Models;

use App\Models\Traits\CanBeDefault;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use CanBeDefault;

    protected $fillable = [
        'user_id',
        'card_type',
        'last_four',
        'provider_id',
        'type',
        'default'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeWithUser(Builder $query, $value){
        $query->where('user_id', $value);
    }

    public function byTypes(){

        switch($this->type){
            case 'credit_card': return 'CARTÃO DE CRÉDITO';
                break;
            case 'boleto': return 'BOLETO';
                break;
            case 'on_delivery': return 'PAGAMENTO NA ENTREGA';
                break;

        }
        return null;
    }

    public function setCardTypeAttribute($value) {
        $this->attributes['card_type'] = strtolower($value);
    }
}
