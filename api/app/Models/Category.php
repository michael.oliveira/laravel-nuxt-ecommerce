<?php

namespace App\Models;

use App\Models\Traits\HasParents;
use App\Models\Traits\IsOrdered;
use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    use HasParents, IsOrdered;

    protected $guarded = [];

    protected $fillable = [
        'name',
        'slug'
    ];

    //protected $table = 'category';

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
