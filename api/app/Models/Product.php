<?php

namespace App\Models;

use App\Models\Traits\CanBeScoped;
use App\Models\Traits\CreatingAttributes;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\HasPrice;

class Product extends Model
{
    use
        Searchable,
        CanBeScoped,
        HasPrice;

    protected $fillable = ['quantity'];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 10,
            'products.description' => 2,
        ],
    ];

    public function inStock()
    {
        return $this->stockCount() > 0;
    }

    public function stockCount()
    {
        return $this->variations->sum(function ($variation) {
            return $variation->stockCount();
        });
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function presentPrice()
    {
        return $this->formattedPrice;
    }

    public function scopeMightAlsoLike($query)
    {
        return $query->inRandomOrder()->take(4);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function attributes() {
        return $this->hasMany(ProductAttribute::class);
    }

    public function variations()
    {
        return $this->hasMany(ProductVariation::class)->orderBy('order', 'asc');
    }
}
