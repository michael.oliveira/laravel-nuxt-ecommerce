<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $fillable = [
        'name',
        'position',
    ];

    public function product() {
        return $this->belongsToMany(Product::class);
    }

    public function values() {
        return $this->hasMany(ProductAttributeValue::class);
    }
}
