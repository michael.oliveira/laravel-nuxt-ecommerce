<?php

namespace App\Models\Traits;

use App\Models\Address;
use App\Models\ShippingMethod;
use App\Models\User;

trait CanBeShippingDefault {

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if ($model->default){

                $model->newQuery()->where('address_id', $model->address_id)
                ->update([
                    'default'   => false
                ]);
            }
        });

        static::updating(function ($model){
            if ($model->default){
                $model->newQuery()->where('address_id', $model->address_id)
                ->update([
                    'default'   => false
                ]);
            }
        });
    }

    public function setDefaultAttribute($value){
        $this->attributes['default'] = ($value === 'true' || $value ? true : false);
    }

}