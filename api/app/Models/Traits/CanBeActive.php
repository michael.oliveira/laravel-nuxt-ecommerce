<?php

namespace App\Models\Traits;

trait CanBeActive {
    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            if ($model->is_active) {
                $model->newQuery()->where('product_attribute_value_id', $model->product_attribute_value_id)
                ->update([
                    'is_active' => false
                ]);
            }
        });

        static::updating(function ($model) {
            if ($model->is_active) {
                $model->newQuery()->where('product_attribute_value_id', $model->product_attribute_value_id)
                ->update([
                    'is_active' => false
                ]);
            }
        });
    }

    public function setIsActiveAttribute($value){
        $this->attributes['is_active'] = ($value === 'true' || $value ? true : false);
    }
}
