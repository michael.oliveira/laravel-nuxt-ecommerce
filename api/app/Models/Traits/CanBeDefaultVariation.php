<?php

namespace App\Models\Traits;

trait CanBeDefaultVariation {

    public static function boot()
    {
        parent::boot();

        static::saving(function($product) {
            $product->product_variation_attributes = json_encode(request('attributes'));
        });

        static::creating(function ($model){
            if ($model->default){

                $model->newQuery()->where('product_id', $model->product_id)
                ->update([
                    'default'   => false
                ]);
            }
        });

        static::updating(function ($model){
            if ($model->default){
                $model->newQuery()->where('product_id', $model->product_id)
                ->update([
                    'default'   => false
                ]);
            }
        });
    }

    public function setDefaultAttribute($value){
        $this->attributes['default'] = ($value === 'true' || $value ? true : false);
    }

}
