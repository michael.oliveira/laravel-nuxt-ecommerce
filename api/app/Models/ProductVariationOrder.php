<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariationOrder extends Model
{
    protected $table = 'product_variation_order';

    protected $fillable = ['order_id', 'product_id', 'quantity'];
}
