<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name', 
        'cpf_cnpj', 
        'doc_type', 
        'address_id'
    ];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
