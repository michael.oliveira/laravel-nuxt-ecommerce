<?php

namespace App\Models;

use App\Models\Traits\CanBeShippingDefault;
use App\Models\Traits\HasPrice;
use Illuminate\Database\Eloquent\Model;
use App\Cart\Money;
use App\Models\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Builder;

class ShippingMethod extends Model
{
    use HasPrice, 
        CanBeShippingDefault, 
        CanBeScoped;

    protected $fillable = ['name', 'delivery_time', 'price', 'default'];

    public function countries(){
        return $this->belongsTo(Country::class);
    }

    // public function addresses(){
    //     return $this->belongsToMany(Address::class);
    // }

    public function addresses(){
        return $this->belongsTo(Address::class);
    }

    public function getId() {
        return $this->id;
    }

    public function getPriceAttribute($value){
        return new Money($value);
    }

}
