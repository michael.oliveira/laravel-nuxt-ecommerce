<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeValue extends Model
{
    protected $fillable = [
        'product_variation_id',
        'product_attribute_id',
        'name',
        'position',
        'image'
    ];

    public function productVariation()
    {
        return $this->belongsTo(ProductVariation::class);
    }

    // public function images() {
    //     return $this->hasMany(ProductVariationImage::class);
    // }

    public function attribute() {
        return $this->belongsTo(ProductAttribute::class, 'product_attribute_id');
    }
}
