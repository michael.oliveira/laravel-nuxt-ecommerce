<?php
namespace App\Models;

class Utils {
    public static function money_fmt($value){
        return 'R$ ' . number_format($value, 2, ',', '.');
    }
}