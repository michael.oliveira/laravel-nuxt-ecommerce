<?php

namespace App\Models;

use App\Cart\Money;
use App\Models\Traits\HasPrice;
use App\Models\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Model;
use App\Models\Collections\ProductVariationCollection;
use App\Models\Traits\CanBeDefaultVariation;
use App\Models\Traits\CreatingAttributes;

class ProductVariation extends Model
{
    use
        HasPrice,
        CanBeScoped,
        CanBeDefaultVariation;

    protected $cartItem;

    public function getPriceAttribute($value)
    {
        if($value === null) {
            return $this->product->price;
        }

        return new Money($value);

    }

    public function parent() {
        return $this->belongsTo(ProductVariation::class, 'parent_id', 'id');
    }

    public function getCartItem()
    {
        return $this->cartItem;
    }

    public function setCartItem($cartItem)
    {
        $this->cartItem = $cartItem;
    }

    public function priceVaries()
    {
        return $this->price->amount() !== $this->product->price->amount();
    }

    public function minStock($count)
    {
        return min($this->stockCount(), $count);
    }

    public function inStock()
    {
        return $this->stockCount() > 0;
    }

    public function stockCount()
    {
        return $this->stock->sum('pivot.stock');
    }

    public function type()
    {
        return $this->hasOne(ProductVariationType::class, 'id', 'product_variation_type_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function stocks(){
        return $this->hasMany(Stock::class);
    }

    public function images() {
        return $this->hasMany(ProductVariationImage::class);
    }

    public function attributes() {
        return $this->belongsToMany(ProductAttributeValue::class, 'product_variation_attributes');
    }

    public function stock()
    {
        return $this->belongsToMany(
            ProductVariation::class, 'product_variation_stock_view'
        )
            ->withPivot([
                'stock',
                'in_stock'
            ]);
    }

    public function newCollection(array $models = [])
    {
        return new ProductVariationCollection($models);
    }
}
