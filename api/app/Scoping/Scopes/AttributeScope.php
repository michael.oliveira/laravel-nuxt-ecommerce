<?php

namespace App\Scoping\Scopes;

use App\Scoping\Contracts\Scope;
use Illuminate\Database\Eloquent\Builder;

class AttributeScope implements Scope
{
    public function apply(Builder $builder, $value)
    {
        return $builder->whereHas('attributes', function ($builder) use ($value){
            $builder->where('product_variation_id', $value);
        });
    }
}
