<?php

namespace App\Scoping\Scopes;

use App\Scoping\Contracts\Scope;
use Illuminate\Database\Eloquent\Builder;

class AddressScope implements Scope {
    
    public function apply(Builder $builder, $value)
    {
        return $builder->whereHas('addresses', function($builder) use($value) {
            $builder->where('id', $value);
        });  
    }
}