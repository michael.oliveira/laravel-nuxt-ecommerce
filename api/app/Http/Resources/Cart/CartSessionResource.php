<?php

namespace App\Http\Resources\Cart;

use App\Http\Resources\ProductVariationResource;
use App\Models\ProductVariation;
use Illuminate\Http\Resources\Json\JsonResource;

class CartSessionResource extends ProductVariationResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'products'  => new CartSessionProductVariationResource($this->resource)
            // 'products'  => $this->resource
        ];
    }

    public function getProducts()
    {
    }
}
