<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use MercadoPago;

class PaymentMethodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'card_type' => $this->card_type,
            'last_four' => $this->last_four,
            'type'  => $this->byTypes(),
            'isType'    => $this->type,
            'card'  => $this->type === 'credit_card' ? $this->getCardDetails($request->user(), $this->provider_id) : null,
            'default'   => $this->default
        ];
    }

    public function getCardDetails($user, $cardId) {
        $customer = MercadoPago\Customer::find_by_id($user->gateway_customer_id);
        $cards = $customer->cards;
        $card = null;

        foreach($cards as $customerCard) {
            if ($customerCard->id == $cardId)
            {
                $card = $customerCard;
            }
        }

        return $card;
    }
}
