<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'doc_type'  =>$this->doc_type,
            'cpf_cnpj'  => $this->cpf_cnpj
            // 'address'   => new AddressResource($this->address)
        ];
    }
}
