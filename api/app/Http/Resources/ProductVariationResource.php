<?php

namespace App\Http\Resources;

use App\Cart\CartSession;
use App\Cart\Money;
use App\Models\ProductVariation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as iCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource instanceof CartSession){

            $collect = Collection::make($this->resource->getContent()->keyBy('id')->map(function($product) {
                $productResource = $this->getProduct($product->id);
                $productResource->setCartItem($this->resource->getCartItem($product->id));
                return $productResource;
            }));

            if ($collect instanceof Collection) {
                return ProductVariationResource::collection($collect);
            }

        }
        if ($this->resource instanceof Collection) {
            return ProductVariationResource::collection($this->resource);
        }

        return [
            'id' => $this->id,
            'name'  => $this->name,
            'price' => $this->price->formatted(),
            'order' => $this->order,
            'price_varies'  => $this->priceVaries(),
            'stock_count'   => (int) $this->stockCount(),
            'sku'   => $this->sku,
            // 'type'  => $this->type->name,
            // 'attributes'    => json_decode($this->product_variation_attributes),
            'product'   => $this->product->only(['id', 'name', 'slug']),
            'attributes'    => ProductAttributeValueResource::collection(
                $this->attributes
            )->groupBy('attribute.name'),
            'images'    => $this->images,
            'in_stock'  => $this->inStock(),
            'default'   => $this->default,
            'quantity'  => $this->getCartItem() ? $this->getCartItem()->qty : null,
            'total' => $this->getCartItem() ? $this->getTotal()->formatted() : null
        ];
    }

    public function getProduct($id){
        return ProductVariation::find($id);
    }

    protected function getTotal()
    {
        return new Money($this->getCartItem()->subTotal());
    }

}
