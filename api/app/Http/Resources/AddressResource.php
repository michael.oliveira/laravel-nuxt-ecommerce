<?php

namespace App\Http\Resources;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource instanceof Collection)
        {
            dd('um collection');
        }
        return [
            'id'    => $id = $this->id,
            'name'  => $this->name,
            'address_1' => $this->address_1,
            'city'  => $this->city,
            'postal_code'   => $this->postal_code,
            'country'   => new CountryResource($this->country),
            'customer'  => Customer::where('address_id', $this->id)->first(),
            'default'   => $this->default,
            'shippingMethods'   => ShippingMethodResource::collection($this->shippingMethods)
        ];
    }
}
