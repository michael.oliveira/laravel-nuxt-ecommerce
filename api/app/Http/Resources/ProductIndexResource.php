<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'slug'  => $this->slug,
            'price' => $this->price->formatted(),
            'quantity'  => $this->quantity,
            'description'   => $this->description,
            'details'   => $this->details,
            'category'  => new CategoryResource($this->categories->first()),
            'image' => $this->image,
            'images'    => $this->images,
            'attributes'    => ProductAttributeValueResource::collection($this->attributes)->groupBy('name'),
            'featured'  => $this->featured,
            'stock_count'   => $this->stockCount(),
            'in_stock'  => $this->inStock()
        ];
    }
}
