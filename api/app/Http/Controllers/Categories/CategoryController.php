<?php

namespace App\Http\Controllers\Categories;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        return CategoryResource::collection(
            Category::with(['children', 'children.children', 'children.children.children'])->parents()->ordered()->get()
        );
    }

    public function show(Category $category) {
        return new CategoryResource($category);
    }
}
