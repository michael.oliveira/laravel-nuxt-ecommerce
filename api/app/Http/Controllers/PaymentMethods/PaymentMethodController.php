<?php

namespace App\Http\Controllers\PaymentMethods;

use App\Cart\Payments\Gateway;
use App\Cart\Payments\Gateways\MPGateway;
use App\Http\Resources\PaymentMethodResource;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\QueryException;

class PaymentMethodController extends Controller
{
    protected $gateway;

    public function __construct(Gateway $gateway)
    {
        $this->middleware('auth:sanctum');
        $this->gateway = $gateway;
    }

    public function getCredentials()
    {
        return response()->json([
            'data'  => [
                'email' => env('PAGSEGURO_EMAIL'),
                'token' => env('PAGSEGURO_TOKEN')
            ]
        ]);
    }

    public function getMpCredentials(){
        return response()->json([
            'data'  => config('mercadopago.sand_box') == true ? config('mercadopago.test') : config('mercadopago.production')
        ]);
    }

    public function index(Request $request)
    {
        return PaymentMethodResource::collection($request->user()->paymentMethods);
    }

    public function store(Request $request)
    {
        try {
            if ($request->id){
                $payment = $this->gateway
                ->withUser($request->user())
                ->createCustomer($request)
                ->addCard($request);

                $paymentMethod = PaymentMethod::create([
                    'user_id'   => $request->user()->id,
                    'last_four' => $payment->last_four_digits,
                    'provider_id'   => $payment->id,
                    'type'  => $payment->payment_method->payment_type_id,
                    'card_type' =>  $payment->payment_method->id,
                    'default'   => true
                ]);
            } else {
                $paymentMethod = PaymentMethod::create([
                    'user_id'   => $request->user()->id,
                    'last_four' => '',
                    'provider_id'   => $request->type,
                    'type'  => $request->type,
                    'card_type' =>  $request->type,
                    'default'   => true
                ]);

            }
        } catch(QueryException $exp){
            $errorCode = $exp->getCode();
            if ($errorCode == 23505){
                $paymentMethodSelected =
                PaymentMethod::where('provider_id', $request->type)->withUser($request->user()->id);
                $paymentMethodSelected->update(['default' => true]);
                return response()->json([
                    'error' => true,
                    'data'   => new PaymentMethodResource($paymentMethodSelected->first())
                ]);
            }
        }

        return new PaymentMethodResource($paymentMethod);
    }

    public function changeDefault(PaymentMethod $paymentMethod, Request $request){
        $paymentMethod->update(['default' => $request->default]);

        return new PaymentMethodResource($paymentMethod);
    }
}
