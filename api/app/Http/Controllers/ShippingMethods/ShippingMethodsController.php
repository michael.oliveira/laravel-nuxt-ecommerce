<?php

namespace App\Http\Controllers\ShippingMethods;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Addresses\ShippingMethodStoreRequest;
use App\Http\Resources\ShippingMethodResource;
use App\Models\Address;
use App\Models\ShippingMethod;

class ShippingMethodsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    public function index()
    {
        return ShippingMethodResource::collection(ShippingMethod::get());
    }

    public function getFrete(Request $request){
        // $this->authorize('show', $request);

        $dados = [
            'tipo'              => 'sedex,pac',
            'formato'           => 'caixa', // opções: `caixa`, `rolo`, `envelope`
            'cep_destino'       => $request->cep,
            'cep_origem'        => '68903359',
            //'empresa'         => '',
            //'senha'           => '',
            'peso'              => '1',
            'comprimento'       => '16',
            'altura'            => '11',
            'largura'           => '11',
            'diametro'          => '0',
            // 'mao_propria'       => '1', // Náo obrigatórios
            // 'valor_declarado'   => '1', // Náo obrigatórios
            // 'aviso_recebimento' => '1', // Náo obrigatórios
        ];
        $fretes = \Correios::frete($dados);
        $shippingMethod = [];
        // dd($fretes);
        foreach($fretes as $frete){

            $shippingMethod[] = [
                'id'    => $frete['codigo'],
                'name'  => $frete['codigo'] == 4510 ? 'PAC' : 'SEDEX',
                'delivery_time' => $frete['prazo'],
                'price' => $frete['valor']
            ];
        }

            // $address->shippingMethods()->save($shippingMethod);

        return response()->json($shippingMethod);
    }

    public function update(ShippingMethod $shippingMethod, Request $request){
        // dd($request->default);
        $shippingMethod->update(['default'  => $request->default]);

        return new ShippingMethodResource($shippingMethod);
    }
}
