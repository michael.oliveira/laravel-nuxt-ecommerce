<?php

namespace App\Http\Controllers\Cart;

use App\Http\Requests\Cart\CartUpdateRequest;
use \Cart;
use App\Cart\CartSession;
use App\Http\Requests\Cart\CartStoreRequest;
use App\Http\Resources\Cart\CartSessionResource;
use App\Models\ProductVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartSessionController extends Controller
{
    public function index(CartSession $cart)
    {
        return (new CartSessionResource($cart))
            ->additional([
                'meta'  => $this->meta($cart)
            ]);
    }

    public function store(CartStoreRequest $request, CartSession $cart)
    {
        $cart->add($request->products);
    }

    public function destroy(Request $request, CartSession $cart)
    {
        $cart->delete($request->productVariation);
    }

    public function clearCart(CartSession $cart)
    {
        $cart->empty();
    }

    protected function meta(CartSession $cart)
    {
        return [
            'empty'    => $cart->isEmpty(),
            'subtotal'  => $cart->subtotal()->formatted(),
            'total'  => $cart->total()->formatted(),
            'changed'   => $cart->hasChanged()
        ];
    }

    public function update(ProductVariation $productVariation, CartUpdateRequest $request, CartSession $cart)
    {
        $cart->update($productVariation->id, $request->quantity);
    }
}
