<?php

namespace App\Http\Controllers\Cart;

use App\Cart\Cart;
use App\Http\Requests\Cart\CartStoreRequest;
use App\Http\Requests\Cart\CartUpdateRequest;
use App\Http\Resources\Cart\CartResource;
use App\Models\ProductVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ShippingMethodResource;
use App\Models\ShippingMethod;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(Request $request, Cart $cart)
    {
        $cart->sync();

        $request->user()->load([
            'cart.product', 'cart.product.variations.stock', 'cart.stock', 'cart.type'
        ]);

        return (new CartResource($request->user()))
                ->additional([
                    'meta'  => $this->meta($cart, $request)
                ]);
    }

    public function cartSync(Request $request){
            $collection = collect($request->data['products'])->keyBy('id')->map(function($product){
                return [
                    'quantity' => $product['quantity']
                ];

            })->toArray();

            $request->user()->cart()->sync($collection);
    }

    protected function meta(Cart $cart, Request $request)
    {
        return [
            'empty'    => $cart->isEmpty(),
            'subtotal'  => $cart->subtotal()->formatted(),
            'shipping'  => $request->query('shipping_method_id') ? new ShippingMethodResource(ShippingMethod::find($request->query('shipping_method_id'))) : '' ,
            'total'  => $cart->withShipping($request->query('shipping_method_id'))->total()->formatted(),
            'changed'   => $cart->hasChanged()
        ];
    }

    public function store(CartStoreRequest $request, Cart $cart)
    {
        $cart->add($request->products);
    }

    public function update(ProductVariation $productVariation, CartUpdateRequest $request, Cart $cart)
    {
        $cart->update($productVariation->id, $request->quantity);
    }

    public function destroy(ProductVariation $productVariation, Cart $cart)
    {
        $cart->delete($productVariation->id);
    }
}
