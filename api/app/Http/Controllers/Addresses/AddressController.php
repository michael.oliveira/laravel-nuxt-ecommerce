<?php

namespace App\Http\Controllers\Addresses;

use App\Http\Requests\Addresses\AddressStoreRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\ShippingMethodResource;
use App\Models\Customer;
use App\Models\PaymentMethod;
use App\Models\ShippingMethod;
use Exception;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function index(Request $request)
    {
        $perPage = (int) request('per_page', 5);

        try {
            return AddressResource::collection(
                $request->user()->addresses()->latest()
                ->paginate($perPage)
            );
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function store(AddressStoreRequest $request)
    {
        $address = Address::make($request->only([
            'name',
            'address_1',
            'address_2',
            'city',
            'state',
            'postal_code',
            'complement',
            'number',
            'neighborhood',
            'country_id',
            'default'
        ]));

        $request->user()->addresses()->save($address);
        Customer::create([
            'name'  => $address->name,
            'address_id'   => $address->id,
            'doc_type'  => $request->doc_type,
            'cpf_cnpj'  => $request->cpf_cnpj
        ]);

        $this->storeShippings($address);

        return new AddressResource($address);
    }

    public function show($id) {
        return new AddressResource(Address::find($id));
    }

    public function changeDefault(Address $address, Request $request){
        $address->update(['default' => $request->default]);

        return new AddressResource($address);
    }

    public function update(Request $request){
        $address = Address::find($request->id);

        $address->update($request->only([
            'name', 'address_1', 'city', 'postal_code', 'country_id', 'default'
        ]));

        return new AddressResource($address);
    }

    public function storeShippings(Address $address){
        $dados = [
            'tipo'              => 'sedex,pac',
            'formato'           => 'caixa', // opções: `caixa`, `rolo`, `envelope`
            'cep_destino'       => $address->postal_code,
            'cep_origem'        => '68903359',
            //'empresa'         => '',
            //'senha'           => '',
            'peso'              => '1',
            'largura'           => '11',
            'comprimento'       => '16',
            'altura'            => '11',
            'diametro'          => '0',
        ];

        $fretes = \Correios::frete($dados);
        foreach($fretes as $key => $frete){
            $shippingMethod = ShippingMethod::make([
                'name'  => $frete['codigo'] == 4510 ? 'PAC' : 'SEDEX',
                'delivery_time' => $frete['prazo'],
                'price' => str_replace(".", "", number_format($frete['valor'],2)),
                'default'   => true
            ]);

            $address->shippingMethods()->save($shippingMethod);
        }
    }
}
