<?php

namespace App\Http\Controllers\Products;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Scoping\Scopes\CategoryScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductIndexResource;
use App\Http\Resources\ProductVariationResource;
use App\Models\ProductAttribute;
use App\Models\ProductVariation;
use App\Scoping\Scopes\AttributeScope;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['api']);
    }

    public function index()
    {
        $perPage = request('perPage', 10);
        $products = Product::with(['variations.stock'])->withScopes($this->scopes())->paginate($perPage);

        return ProductIndexResource::collection($products);
    }

    public function featuredProducts() {
        $products = Product::where('featured', true)->get();

        return ProductIndexResource::collection($products);
    }

    public function searchVariants(Request $request) {

        $builder = [];
        // return response()->json($request->data);
        foreach($request->data as $key => $attribute) {
            if (count($request->data) > 1) {
                $builder[] =  "product_variation_attributes ->> '" . $attribute['attribute_name'] . "' = '" . $attribute['name'] . "'";
            } else {
                $builder =  "product_variation_attributes ->> '" . $attribute['attribute_name'] . "' = '" . $attribute['name'] . "'";
            }

        }

        $sql = (count($request->data) > 1) ? (implode(" AND ", $builder)) : $builder;

        $variations = ProductVariation::whereRaw($sql)->get();

        return ProductVariationResource::collection($variations);
    }

    public function show(Product $product)
    {
        $product->load(['variations.type', 'variations.stock', 'attributes.values', 'variations.images']);
        return new ProductResource($product);
    }

    public function scopes()
    {
        return [
            'category'  => new CategoryScope(),
            'variations'    => new AttributeScope()
        ];
    }
}
