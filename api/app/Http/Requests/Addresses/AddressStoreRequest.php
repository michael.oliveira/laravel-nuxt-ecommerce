<?php

namespace App\Http\Requests\Addresses;

use Illuminate\Foundation\Http\FormRequest;

class AddressStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address_1' => 'required|min:1',
            'city' => 'required',
            'postal_code' => 'required',
            'country_id' => 'required|exists:countries,id',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O campo nome do endereço é obrigatório'
        ];
    }
}
