<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    // protected function decrypt( Request $request ) {
    //     foreach ($request->cookies as $key => $cookie) {
    //         if ($this->isDisabled($key)) {
    //             continue;
    //         }

    //         try {
    //             $request->cookies->set($key, $this->decryptCookie($key, urldecode($cookie))); //Make sure urldecode the cookie before decryption
    //         } catch (DecryptException $e) {
    //             dd('Cookie: Decoded: ', urldecode($cookie));
    //             $request->cookies->set($key, null);
    //         }
    //     }

    //     return $request;
    // }
}
