<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\CookieValuePrefix;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // '/api/products-variants',
        // '/login'
    ];

    /**
     * Determine if the session and input CSRF tokens match.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    // protected function tokensMatch($request)
    // {
    //     base64_decode($request->header('X-XSRF-TOKEN'));

    //     $token = $this->getTokenFromRequest($request);

    //     // dd(hash_equals($request->session()->token(), $token));

    //     return is_string($request->session()->token()) &&
    //            is_string($token) &&
    //            hash_equals($request->session()->token(), $token);
    // }
}
