<?php

namespace App\Http\Middleware;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful as BaseEnsureFrontendRequestsAreStatefulAlias;

class EnsureFrontendRequestsAreStateful extends BaseEnsureFrontendRequestsAreStatefulAlias
{

    /**
     * Determine if the given request is from the first-party application frontend.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public static function fromFrontend($request)
    {
        $domain = config('domain.frontend');

        // $domain = $request->headers->get('referer') ?: $request->headers->get('origin');

        $domain = Str::replaceFirst('https://', '', $domain);
        $domain = Str::replaceFirst('http://', '', $domain);
        $domain = Str::endsWith($domain, '/') ? $domain : "{$domain}/";

        $stateful = array_filter(config('sanctum.stateful', []));

        return Str::is(Collection::make($stateful)->map(function ($uri) {
            return trim($uri).'/*';
        })->all(), $domain);
    }
}
