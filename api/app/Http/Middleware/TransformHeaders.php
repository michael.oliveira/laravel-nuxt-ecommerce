<?php

namespace App\Http\Middleware;

use Closure;

class TransformHeaders {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     */
    public function handle($request, Closure $next) {
        $cookie_name = 'XSRF-TOKEN';
        $token_cookie = $request->cookie($cookie_name);
        if ($token_cookie) {
            $request->headers->add(["X-$cookie_name" => $token_cookie]);
        }

        return $next($request);
    }
}
