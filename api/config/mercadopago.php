<?php

return [
    'sand_box'  => true,
    'test'  => [
        'key'   => 'TEST-1f855d4a-4d9d-4a1c-a83b-28d5ec373682',
        'token'  => 'TEST-749003265220886-102013-e625c5340447186dc9a2f855c417c29e-94300472'
    ],
    'production'  => [
        'key'   => 'APP_USR-ca682ace-c5c7-4dd0-9986-74df91dee4e3',
        'token' => 'APP_USR-749003265220886-102013-3aafac4b35f8f6fea0144eee1ef10b62-94300472'
    ]
];