<?php

use App\Models\Address;
use App\Models\ShippingMethod;
use Faker\Generator as Faker;

$factory->define(ShippingMethod::class, function (Faker $faker) {
    return [
        'name'  => 'Correios',
        'price' => 1000,
        'delivery_time' => 5,
        'address_id'   => factory(Address::class)->create()->id
    ];
});
