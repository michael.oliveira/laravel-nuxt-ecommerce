<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductVariationAttributeStockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW product_variation_attribute_stock_view AS
            SELECT pv.product_id,
                pv.id AS product_variation_id,
                pa.attribute,
                pa.attribute_value,
                COALESCE((sum(stocks.quantity) - COALESCE(sum(product_variation_order.quantity), (0)::numeric)), (0)::numeric) AS stock,
                    CASE
                        WHEN (COALESCE((sum(stocks.quantity) - COALESCE(sum(product_variation_order.quantity), (0)::numeric)), (0)::numeric) > (0)::numeric) THEN true
                        ELSE false
                    END AS in_stock
                FROM ((product_variations pv
                LEFT JOIN ( SELECT pva.product_variation_id AS id,
                        pva.product_attribute_value_id,
                    pattr.name AS attribute,
                    pav.name AS attribute_value
                   FROM product_variation_attributes pva
                INNER JOIN product_attribute_values pav ON pav.id = pva.product_attribute_value_id
                INNER JOIN product_attributes pattr ON pattr.id = pav.product_attribute_id
                  GROUP BY pva.product_variation_id,
                       pva.product_attribute_value_id,
                       pattr.name,
                       pav.name) pa USING (id))
                LEFT JOIN ( SELECT s.product_variation_id AS id,
                    sum(s.quantity) AS quantity
                   FROM stocks s
                  GROUP BY s.product_variation_id) stocks USING (id)
                LEFT JOIN ( SELECT pvo.product_variation_id AS id,
                    sum(pvo.quantity) AS quantity
                   FROM product_variation_order pvo
                  GROUP BY pvo.product_variation_id) product_variation_order USING (id))
            GROUP BY pv.product_id, pv.id, pa.attribute, pa.attribute_value
            ORDER BY product_id, pa.attribute;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS product_variation_attribute_stock_view");
    }
}
