<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressDetailOnAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('neighborhood')->nullable();
            $table->string('state', 2)->nullable();
            $table->string('complement')->nullable();
            $table->string('number', 20)->nullable();
            $table->string('reference')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('neighborhood');
            $table->dropColumn('state', 2);
            $table->dropColumn('complement');
            $table->dropColumn('number', 20);
            $table->dropColumn('reference');
        });
    }
}
