<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariationAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variation_attributes', function (Blueprint $table) {
            $table->integer('product_variation_id')->unsigned()->index();
            $table->integer('product_attribute_value_id')->unsigned()->index();
            $table->foreign('product_variation_id')->references('id')->on('product_variations');
            $table->foreign('product_attribute_value_id')->references('id')->on('product_attribute_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variation_attributes');
    }
}
