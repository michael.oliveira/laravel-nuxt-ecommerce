<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductVariationStockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW product_variation_attribute_view AS
            SELECT pv.product_id,
            pv.id AS product_variation_id,
<<<<<<< HEAD
            COALESCE((sum(stocks.quantity) - COALESCE(sum(product_variation_order.quantity), (0)::numeric)), (0)::numeric) AS stock,
                CASE
                    WHEN (COALESCE((sum(stocks.quantity) - COALESCE(sum(product_variation_order.quantity), (0)::numeric)), (0)::numeric) > (0)::numeric) THEN true
                    ELSE false
                END AS in_stock
        FROM ((product_variations pv
            LEFT JOIN ( SELECT s.product_variation_id AS id,
                    sum(s.quantity) AS quantity
                FROM stocks s
                GROUP BY s.product_variation_id) stocks USING (id))
            LEFT JOIN ( SELECT pvo.product_variation_id AS id,
                    sum(pvo.quantity) AS quantity
                FROM product_variation_order pvo
                GROUP BY pvo.product_variation_id) product_variation_order USING (id))
        GROUP BY pv.id;
=======
         	COALESCE((sum(stocks.quantity) - COALESCE(sum(product_variation_order.quantity), (0)::numeric)), (0)::numeric) AS stock,
                  CASE
                      WHEN (COALESCE((sum(stocks.quantity) - COALESCE(sum(product_variation_order.quantity), (0)::numeric)), (0)::numeric) > (0)::numeric) THEN true
                      ELSE false
                  END AS in_stock

           FROM ((product_variations pv
         		LEFT JOIN ( SELECT s.product_variation_id AS id,
                     sum(s.quantity) AS quantity
                    FROM stocks s
                   GROUP BY s.product_variation_id) stocks USING (id)
         		LEFT JOIN ( SELECT pvo.product_variation_id AS id,
                     sum(pvo.quantity) AS quantity
                    FROM product_variation_order pvo
                   GROUP BY pvo.product_variation_id) product_variation_order USING (id)
                ))
        GROUP BY pv.product_id, pv.id;
>>>>>>> bf8a0cd47e96b2a0d4f40378bbe107d3c8de9a62
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS product_variation_stock_view");
    }
}
