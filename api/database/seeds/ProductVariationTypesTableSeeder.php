<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductVariationType;

class ProductVariationTypesTableSeeder extends Seeder
{
    public function run()
    {
        ProductVariationType::insert([
            ['name'  => 'T-shirt', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Short', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Jeans', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Swimwear', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Desktop PC', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Desktop Complete', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Notebook', 'created_at' => now(), 'updated_at' => now()],
            ['name'  => 'Cell Phone', 'created_at' => now(), 'updated_at' => now()]
        ]);
    }
}
