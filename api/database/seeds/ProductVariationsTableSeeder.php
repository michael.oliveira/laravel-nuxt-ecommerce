<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductVariation;

class ProductVariationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductVariation::insert([
            ['name'  => '32GB', 'product_variation_type_id' => 1, 'product_id' => rand(6,7), 'price' => rand(80000, 120000)],
            ['name'  => '64GB', 'product_variation_type_id' => 1, 'product_id' => rand(6,7), 'price' => rand(80000, 120000)],
            ['name'  => '128GB', 'product_variation_type_id' => 2, 'product_id' => rand(6,7), 'price' => rand(80000, 120000)],
            ['name'  => '32GB', 'product_variation_type_id' => 2, 'product_id' => rand(6,7), 'price' => rand(80000, 120000)],
            ['name'  => '64GB', 'product_variation_type_id' => 3, 'product_id' => rand(6,7), 'price' => rand(80000, 120000)],
            ['name'  => '128GB', 'product_variation_type_id' => 3, 'product_id' => rand(6,7), 'price' => rand(80000, 120000)]
        ]);
    }
}
