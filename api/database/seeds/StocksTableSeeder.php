<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Stock;

class StocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stock::insert([
            ['quantity' => 3, 'product_variation_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['quantity' => 7, 'product_variation_id' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['quantity' => 4, 'product_variation_id' => 3, 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
