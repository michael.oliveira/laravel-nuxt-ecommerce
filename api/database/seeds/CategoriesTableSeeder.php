<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            /* ROOT */
            ['name' => 'Fashion', 'slug' => 'fashion', 'parent_id' => NULL, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Man', 'slug' => 'man', 'parent_id' => 1, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Woman', 'slug' => 'woman', 'parent_id' => 1, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Clothes', 'slug' => 'man-clothes', 'parent_id' => 2, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Pants', 'slug' => 'man-pants', 'parent_id' => 2, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Briefs', 'slug' => 'man-briefs', 'parent_id' => 2, 'order' => 3, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Sport', 'slug' => 'man-sport', 'parent_id' => 2, 'order' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Accessories', 'slug' => 'man-accessories', 'parent_id' => 2, 'order' => 5, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Clothes', 'slug' => 'woman-clothes', 'parent_id' => 3, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Pants', 'slug' => 'woman-pants', 'parent_id' => 3, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Lingerie', 'slug' => 'woman-lingerie', 'parent_id' => 3, 'order' => 3, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Sport', 'slug' => 'woman-sport', 'parent_id' => 3, 'order' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Handbag', 'slug' => 'woman-handbag', 'parent_id' => 3, 'order' => 5, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Kids', 'slug' => 'kids', 'parent_id' => 1, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            
            /* ELETRONICOS */
            ['name' => 'Eletrônicos', 'slug' => 'eletronicos', 'parent_id' => NULL, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'TV', 'slug' => 'eletronicos-tv', 'parent_id' => 15, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Audio', 'slug' => 'eletronicos-audio', 'parent_id' => 15, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Videos Games', 'slug' => 'eletronicos-videogames', 'parent_id' => 15, 'order' => 3, 'created_at' => now(), 'updated_at' => now()],
            /* CELULARES E TELEFONES */
            ['name' => 'Telefonia', 'slug' => 'telefonia', 'parent_id' => NULL, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Celulares', 'slug' => 'telefonia-celulares', 'parent_id' => 19, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Tablets', 'slug' => 'telefonia-tablet', 'parent_id' => 19, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Acessórios', 'slug' => 'telefonia-acessorios', 'parent_id' => 19, 'order' => 3, 'created_at' => now(), 'updated_at' => now()],
            /* INFORMATICA */
            ['name' => 'Informática', 'slug' => 'informatica', 'parent_id' => NULL, 'order' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Computadores', 'slug' => 'informatica-computadores', 'parent_id' => 23, 'order' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Notebooks', 'slug' => 'informatica-notebooks', 'parent_id' => 23, 'order' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Peças e Periféricos', 'slug' => 'informatica-pecas', 'parent_id' => 23, 'order' => 3, 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
