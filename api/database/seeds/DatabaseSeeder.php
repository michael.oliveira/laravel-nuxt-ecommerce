<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductVariationTypesTableSeeder::class);
        $this->call(ProductVariationsTableSeeder::class);
        $this->call(StocksTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(UersTableSeeder::class);
    }
}
