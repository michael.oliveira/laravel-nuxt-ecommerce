<?php return array (
  'App\\Providers\\EventServiceProvider' => 
  array (
    'Illuminate\\Auth\\Events\\Registered' => 
    array (
      0 => 'Illuminate\\Auth\\Listeners\\SendEmailVerificationNotification',
    ),
    'App\\Events\\Order\\OrderCreated' => 
    array (
      0 => 'App\\Listeners\\Order\\ProccessPayment',
      1 => 'App\\Listeners\\Order\\EmptyCart',
    ),
  ),
);