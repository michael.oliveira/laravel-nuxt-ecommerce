<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('categories', 'Categories\CategoryController');
Route::resource('products', 'Products\ProductController');
Route::resource('addresses', 'Addresses\AddressController');
Route::resource('countries', 'Countries\CountryController');
Route::resource('orders', 'Orders\OrderController');
Route::resource('payment-methods', 'PaymentMethods\PaymentMethodController');
Route::resource('shipping-methods', 'ShippingMethods\ShippingMethodsController', [
    'parameters'    => [
        'shipping'   => 'shippingMethod'
    ]
]);

Route::post('address-change-default/{address}', 'Addresses\AddressController@changeDefault');

Route::post('payment-change-default/{paymentMethod}', 'PaymentMethods\PaymentMethodController@changeDefault');

Route::post('shipping/frete', 'ShippingMethods\ShippingMethodsController@getFrete');

Route::get('addresses/{address}/shipping', 'Addresses\AddressShippingController@action');

Route::get('payment-credentials', 'PaymentMethods\PaymentMethodController@getCredentials');

Route::get('featured-products', 'Products\ProductController@featuredProducts');

Route::get('getToken', 'HomeController@index');

Route::post('products-variants', 'Products\ProductController@searchVariants');

Route::get('mp-credentials', 'PaymentMethods\PaymentMethodController@getMpCredentials');

Route::post('cart-sync', 'Cart\CartController@cartSync');

Route::group(['prefix'  => 'auth'], function () {
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
    //    Route::get('me', 'Auth\MeController@action');
});


Route::resource('cart', 'Cart\CartController', [
    'parameters'    => [
        'cart'  => 'productVariation'
    ]
]);

Route::resource('cart-session', 'Cart\CartSessionController', [
    'parameters'    => [
        'cart-session'  => 'productVariation'
    ]
]);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
