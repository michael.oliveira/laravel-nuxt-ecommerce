# Project E-commerce complete Frontend (Nuxtjs) and Backend (Laravel)

Projeto e-commerce completo com Frontend e API, criados inicialmente para fins não comerciais pois está em fase de desenvolvimento, com o uso do MercadoPago como gateway de pagamento, também implementação de recursos e consulta ao sistema dos correios para obtenção dos custos de frete.

Em breve será lançando um curso do processo completo da criação deste projeto, desde sua concepção até o deploy

# Instalação

- git clone https://gitlab.com/michael.oliveira/laravel-nuxt-ecommerce.git
- cd laravel-nuxt-ecommerce

##  Laravel with dependencies

- cd api
- composer install (macos) // php composer.phar install (windows)

Alterar o arquivo ".env_sample" para ".env", em seguida execute o comando 
abaixo para realizar a instalação da API

- php artisan ecommerce:install --force

### Configure AWS Labda to ElasticSearch

```
.env

ELASTICSEARCH_HOST=YOUR-ELASTICSEARCH-LAMBDA-SERVICE
```

### Configuration payment method MercadoPago

```
config/mercadopago.php

return [
    'sand_box'  => true,
    'test'  => [
        'key'   => 'TEST-YOUR-KEY',
        'token'  => 'TEST-YOUR-TOKEN'
    ],
    'production'  => [
        'key'   => 'APP_USR-YOUR-KEY',
        'token' => 'APP_USR-YOUR-TOKEN'
    ]
];
```

### php.ini

Extensões do PHP necessárias para rodar a API

```
extension=curl
extension=fileinfo
extension=intl
extension=mbstring
extension=openssl
extension=pdo_pgsql
```

 ## Laravel .env 
 ```
APP_URL=http://127.0.0.1
SESSION_DOMAIN=127.0.0.1
SANCTUM_STATEFUL_DOMAINS=127.0.0.1:3000
```

## Creating user
```
App\Models\User::factory()->create([
        'name' => 'YOUR-USERNAME', 
        'email' => 'YOUR-EMAIL', 
        'password' => Hash::make('secret')
    ]);
```

## Rodando a API
```
php artisan serve
```

# Laravel Nuxt Sanctum Starter

Instalação do Nuxtjs e dependencies, mas antes certifique-se que já tenha instalado o nodejs
e altere o arquivo ".env_sample" para ".env" de dentro da pasta client.

 ## Nuxt .env
```
PORT=3000
HOST=127.0.0.1
BASE_URL=http://127.0.0.1:3000
API_URL=http://127.0.0.1:8000
```
Configuration
```
npm install

npm run dev
```

# Resultado

<img src="images/img1.png" width="800">
<img src="images/img2.png" width="800">
<img src="images/img7.png" width="800">
<img src="images/img6.png" width="400">
<img src="images/img5.png" width="800">
<img src="images/img4.png" width="800">
<img src="images/img9.png" width="800">

# Desenvolvimento mobile
<img src="images/img3.png" width="400">

